#!/usr/bin/python

# Copyright 2018 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import sys
import codecs
import csv
from argparse import ArgumentParser
import nltk
from gkphrases.phraseindex import PhraseIndex
from gkphrases.stemindex import StemIndex


argp = ArgumentParser(
    description='Extracts keyphrases from text using a set of candidate phrases.'
)
argp.add_argument(
    '-n', '--no_expand', action='store_true', required=False, help='If this '
    'flag is given, only the raw list of candidate phrases will be used; that '
    'is, an expanded list of singular and plural forms will not be generated.  '
    'Has no effect if the stemming method of keyphrase matching is used.'
)
argp.add_argument(
    '-m', '--method', choices=['str', 'stem'], default='stem', required=False,
    help='Specifies the method to use for phrase matching.  The choices are '
    '"str", which is plain string matching (optionally, including plural and '
    'singular variants), and "stem" (the default), which uses Porter\'s '
    'stemming algorithm for phrase matching.'
)
argp.add_argument(
    '-f', '--filter_taxa', action='store_true', required=False, help='If this '
    'flag is given, candidate key phrases that are taxonomic names (as '
    'recognized by Catalog of Life) will be excluded from key phrase analysis.'
)
argp.add_argument(
    '-p', '--phrase_list', type=str, action='append', required=True,
    help='The path to a file of candidate phrases, one phrase per line.  This '
    'can be repeated to specify multiple index files.'
)
argp.add_argument(
    '-t', '--text', type=str, action='append', required=True,
    help='The path to a file of text to analyze.  This can be repeated to '
    'specify multiple input text files.'
)
argp.add_argument(
    '-o', '--fileout', type=str, required=False, default='', help='The path '
    'to an output file for writing the results table.'
)

args = argp.parse_args()

if args.method == 'str':
    pindex = PhraseIndex()
    if args.no_expand:
        pindex.setGenerateVariants(False)
else:
    pindex = StemIndex()

pindex.setFilterTaxonomicNames(args.filter_taxa)

# Build the phrase index.
phrasefilecnt = 0
for phrasefile in args.phrase_list:
    phrasefilecnt += 1
    print 'Reading candidate phrases from "{0}" ({1} of {2})...'.format(
        phrasefile, phrasefilecnt, len(args.phrase_list)
    )
    with codecs.open(phrasefile, encoding='utf-8') as fin:
        for line in fin:
            phrase = line.strip()
            pindex.addPhrase(phrase)

# Initialize a dictionary for tracking matching phrases and frequency counts.
keyphrases = {}

# Track the total number of keyphrase usages.
usagecnt = 0

# Examine all phrases in the source text for possible matches.
sentencecnt = 0
textfilecnt = 0
for textfile in args.text:
    textfilecnt += 1
    print 'Analyzing text in "{0}" ({1} of {2})...'.format(
        textfile, textfilecnt, len(args.text)
    )
    with codecs.open(textfile, encoding='utf-8') as fin:
        for line in fin:
            line = line.strip()
            for sentence in nltk.tokenize.sent_tokenize(line):
                sentencecnt += 1
                matches = pindex.lookupSentence(sentence)

                for phrase in matches:
                    if phrase not in keyphrases:
                        keyphrases[phrase] = 0
                    keyphrases[phrase] += matches[phrase]
                    usagecnt += matches[phrase]

if args.fileout == '':
    fout = sys.stdout
else:
    fout = open(args.fileout, 'w')

# Output the keywords ordered by frequency.
try:
    writer = csv.DictWriter(fout, fieldnames=['Keyphrase', 'Frequency', 'Percentile'])
    writer.writeheader()
    row = {'Keyphrase': '', 'Frequency': 0, 'Percentile': 0}

    cumulativewords = 0

    # Do a double sort, first by frequency then by keyword.
    sortedkeys = sorted(keyphrases.keys())
    for key in sorted(sortedkeys, key=lambda keystr: keyphrases[keystr], reverse=True):
        row['Keyphrase'] = key.encode('utf-8')
        row['Frequency'] = keyphrases[key]

        cumulativewords += keyphrases[key]
        row['Percentile'] = round((float(cumulativewords) / usagecnt) * 100, 2)

        writer.writerow(row)
finally:
    if args.fileout != '':
        fout.close()

print '\nSource candidate phrase list size: {0:,}'.format(pindex.getSourcePhraseCount())
if args.method == 'str':
    print 'Variant candidate phrase list size: {0:,}'.format(pindex.getVariantPhraseCount())
else:
    print 'Stemmed phrase list size: {0:,}'.format(pindex.getStemmedPhraseCount())
print 'Maximum phrase size: {0} words'.format(pindex.getMaxPhraseSize())
print 'Extracted keyphrase list size: {0:,}'.format(len(keyphrases))
print 'Total usage count: {0:,}'.format(usagecnt)

print '\nWords in source text: {0:,}'.format(pindex.getCorpusWordCnt())
print 'Sentences in source text: {0:,}'.format(sentencecnt)
print 'Phrases examined in source text: {0:,}\n'.format(pindex.getCorpusPhraseCnt())

