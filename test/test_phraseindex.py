# Copyright (C) 2018 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from gkphrases.phraseindex import PhraseIndex
import unittest


class TestPhraseIndex(unittest.TestCase):
    """
    Tests the PhraseIndex class.
    """
    def setUp(self):
        self.pi = PhraseIndex()

    def test_addPhraseInternal(self):
        self.assertEqual(0, self.pi.getSourcePhraseCount())
        self.assertEqual(0, self.pi.getVariantPhraseCount())
        self.assertEqual(0, self.pi.getMaxPhraseSize())

        self.pi.setGenerateVariants(False)

        self.pi._addPhraseInternal('Abdomen')
        self.assertEqual(1, self.pi.getSourcePhraseCount())
        self.assertEqual(0, self.pi.getVariantPhraseCount())
        self.assertEqual(1, self.pi.getMaxPhraseSize())
        self.assertEqual({'abdomen'}, self.pi.source_phrases)

        self.pi._addPhraseInternal('abdomen')
        self.assertEqual(1, self.pi.getSourcePhraseCount())
        self.assertEqual(0, self.pi.getVariantPhraseCount())
        self.assertEqual(1, self.pi.getMaxPhraseSize())
        self.assertEqual({'abdomen'}, self.pi.source_phrases)

        self.pi.setGenerateVariants(True)

        self.pi._addPhraseInternal('Abdomen')
        self.assertEqual(1, self.pi.getSourcePhraseCount())
        self.assertEqual(2, self.pi.getVariantPhraseCount())
        self.assertEqual(1, self.pi.getMaxPhraseSize())
        self.assertEqual({'abdomen'}, self.pi.source_phrases)
        self.assertEqual(
            {'abdomens': {'abdomen'}, 'abdoman': {'abdomen'}},
            self.pi.variant_phrases
        )

        self.pi._addPhraseInternal('Abdomens')
        self.assertEqual(2, self.pi.getSourcePhraseCount())
        self.assertEqual(2, self.pi.getVariantPhraseCount())
        self.assertEqual(1, self.pi.getMaxPhraseSize())
        self.assertEqual({'abdomen', 'abdomens'}, self.pi.source_phrases)
        self.assertEqual(
            {
                'abdomens': {'abdomen'}, 'abdoman': {'abdomen'},
                'abdomenss': {'abdomens'}
            },
            self.pi.variant_phrases
        )

        self.pi._addPhraseInternal('Abdominal gills')
        self.assertEqual(3, self.pi.getSourcePhraseCount())
        self.assertEqual(4, self.pi.getVariantPhraseCount())
        self.assertEqual(2, self.pi.getMaxPhraseSize())
        self.assertEqual(
            {'abdomen', 'abdomens', 'abdominal gills'}, self.pi.source_phrases
        )
        self.assertEqual(
            {
                'abdomens': {'abdomen'}, 'abdoman': {'abdomen'},
                'abdomenss': {'abdomens'},
                'abdominal gill': {'abdominal gills'},
                'abdominal gillss': {'abdominal gills'}
            },
            self.pi.variant_phrases
        )

    def test_lookupPhrase(self):
        self.pi._addPhraseInternal('abdomen')
        self.pi._addPhraseInternal('abdominal gills')

        self.assertEqual(['abdomen'], self.pi.lookupPhrase('abdomen'))
        self.assertEqual(['abdomen'], self.pi.lookupPhrase('abdomens'))
        self.assertEqual(
            ['abdominal gills'], self.pi.lookupPhrase('abdominal gills')
        )
        self.assertEqual(
            ['abdominal gills'], self.pi.lookupPhrase('abdominal gill')
        )
        self.assertEqual([], self.pi.lookupPhrase('not found'))

    def test_lookupSentence(self):
        self.pi._addPhraseInternal('abdomen')
        self.pi._addPhraseInternal('abdominal gill')

        self.assertEqual({}, self.pi.lookupSentence('Does not match.'))
        self.assertEqual({'abdomen': 1}, self.pi.lookupSentence('Abdomen.'))
        self.assertEqual(
            {'abdomen': 1},
            self.pi.lookupSentence('All insects have an abdomen.')
        )
        self.assertEqual(
            {'abdomen': 1},
            self.pi.lookupSentence('All insects have abdomens.')
        )
        self.assertEqual(
            {'abdomen': 2},
            self.pi.lookupSentence('Abdomens: All insects have an abdomen.')
        )
        self.assertEqual(
            {'abdomen': 2, 'abdominal gill': 1},
            self.pi.lookupSentence(
                'Abdomens: Abdominal gills are located on the abdomen.'
            )
        )

